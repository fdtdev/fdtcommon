# FDT Common

Common library required by all FDT packages


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.3.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtcommon/src/5.3.0/LICENSE.md)