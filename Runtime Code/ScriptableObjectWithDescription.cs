﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.FDT.Common
{
    public abstract class ScriptableObjectWithDescription : ResetScriptableObject
    {
        [SerializeField, TextArea, FormerlySerializedAs("DeveloperDescription")] protected string _description;
        public virtual bool HasCustomIcon => false;
        public virtual string IconFileName => string.Empty;
    }
}