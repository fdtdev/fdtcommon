﻿using System;
using UnityEngine;

namespace com.FDT.Common
{
    [CreateAssetMenu(menuName = "FDT/Common/ID Asset", fileName = "IDAsset")]
    public class IDAsset : ScriptableObjectWithDescription, IRegistrableID, IEquatable<IDAsset>
    {
        public static bool operator ==(IDAsset obj1, IDAsset obj2)
        {
            if (ReferenceEquals(null, obj1)) 
            {
                return ReferenceEquals(null, obj2);
            }
            else
            {
                if(ReferenceEquals(null, obj2)) 
                {
                    return false;
                }
                else
                {
                    return obj1.name == obj2.name;
                }
            }
        }

        public static bool operator !=(IDAsset obj1, IDAsset obj2)
        {
            return !(obj1 == obj2);
        }
    
        public bool Equals(IDAsset other)
        {
            return other != null && name == other.name;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((IDAsset) obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
