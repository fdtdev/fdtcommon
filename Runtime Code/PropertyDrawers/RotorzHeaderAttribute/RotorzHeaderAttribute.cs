﻿using UnityEngine;
using System;

namespace com.FDT.Common
{
    [AttributeUsage( AttributeTargets.All, Inherited = true, AllowMultiple = true )]
    public class RotorzHeaderAttribute : PropertyAttributeBase
    {
        public string headerText;

        public RotorzHeaderAttribute (string header)
        {
            headerText = header;
        }
    }
}