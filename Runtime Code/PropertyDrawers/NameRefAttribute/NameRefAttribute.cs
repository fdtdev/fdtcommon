﻿using UnityEngine;
using System;
using UnityEngine.Events;

namespace com.FDT.Common
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class NameRefAttribute : PropertyAttributeBase
    {
        public System.Type type;
        public NameRefAttribute () {
            this.type = typeof(UnityEngine.Object);
        }
        public NameRefAttribute (System.Type type) {
            this.type = type;
        }
    }
}