﻿using System;
using UnityEngine;

namespace com.FDT.Common
{
	[AttributeUsage( AttributeTargets.Field, Inherited = true)]
	public class ResourceReferenceAttribute : PropertyAttributeBase
	{
		public string filter = null;
		public Type type;
		
		public ResourceReferenceAttribute()
		{

		}
		public ResourceReferenceAttribute(string filter)
		{
			this.filter = filter;
		}
		public ResourceReferenceAttribute(Type type)
		{
			this.type = type;
		}
		public ResourceReferenceAttribute(string filter, Type type)
		{
			this.filter = filter;
			this.type = type;
		}
	}
}