﻿using UnityEngine;
using System;

namespace com.FDT.Common
{
	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class ButtonEnumAttribute : PropertyAttributeBase
	{
		public ButtonEnumAttribute() { }
	}
}