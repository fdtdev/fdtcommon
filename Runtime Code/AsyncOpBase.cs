﻿using System;
using UnityEngine;

namespace com.FDT.Common
{
    public class AsyncOpBase<T>: CustomYieldInstruction where T:AsyncOpBase<T>
    {
        public Action<string> OnComplete;
        protected bool _isDone = false;

        public bool IsDone
        {
            get { return _isDone; }
            set { _isDone = value;
                OnIsDoneChanged();
            }
        }

        protected virtual void OnIsDoneChanged()
        {
            
        }

        public T SetOnComplete(Action<string> callback)
        {
            OnComplete = callback;
            return this as T;
        }

        public override bool keepWaiting
        {
            get { return !IsDone; }
        }
    }
}