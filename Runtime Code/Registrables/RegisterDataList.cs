using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common
{
// --------------
// Creation Date:   09/02/2020 21:07:18
// Product Name:    FDT Common
// Developers:      FDT Dev
// Company:         FDT Dev
// Description:     
// --------------
    [System.Serializable]
    public class RegisterDataList
    {
        #region Classes and Structs
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Methods
                
        #endregion
    }
}