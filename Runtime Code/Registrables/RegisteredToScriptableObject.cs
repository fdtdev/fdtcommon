﻿using UnityEngine;

namespace com.FDT.Common
{
    // --------------
    // Creation Date:   09/02/2020 17:39:47
    // Product Name:    FDT Common
    // Developers:      FDT Dev
    // Company:         FDT Dev
    // Description:     
    // --------------
    public abstract class RegisteredToScriptableObject<TRegisterer, 
        TRegistrable, TRegistrableId> : RegisteredBase<TRegistrableId>,
        IRegistrable<TRegistrableId>
        where TRegisterer : IRegisterer<TRegistrable, TRegistrableId>
        where TRegistrable : class, IRegistrable<TRegistrableId>
        where TRegistrableId : IRegistrableID
    {
        #region Inspector Fields
        [Header("Protected"), SerializeField] protected TRegisterer _registerList;
        #endregion
 
        #region Properties, Consts and Statics

        #endregion
 
        #region Methods
        protected override void Unregister()
        {
            _registerList.Unregister(((Object) this) as TRegistrable, ReactToUnregistration);
        }

        protected override void Register()
        {
            _registerList.Register(((Object) this) as TRegistrable, ReactToRegistration);
        }
        #endregion
    }
}