﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common
{
    public abstract class ScriptableObjectRegisterer<TRegistrable, TRegistrableID> : ResetScriptableObject,
        IRegisterer<TRegistrable, TRegistrableID> where TRegistrableID : IRegistrableID
        where TRegistrable : IRegistrable<TRegistrableID>
    {
        protected bool _initialized = false;
    
        protected Dictionary<TRegistrableID, TRegistrable> _registeredByID =
            new Dictionary<TRegistrableID, TRegistrable>();

        protected List<TRegistrable> _registered = new List<TRegistrable>();
        public List<TRegistrable> Registered
        {
            get { return _registered; }
        }
        protected int _count = 0;

        public int Count
        {
            get { return _count; }
        }

        private void OnEnable()
        {
            ResetData();
        }
        protected virtual void ResetData()
        {
            if (!_initialized)
            {
                _initialized = true;
                _registered.Clear();
                _registeredByID.Clear();
                _count = 0;
            }
        }
        public TRegistrable this[int idx] => (idx < _registered.Count) ? _registered[idx] : default;
        public TRegistrable this[TRegistrableID id] => (_registeredByID.ContainsKey(id)) ? _registeredByID[id] : default;

        public bool IsType<T>(TRegistrableID registeredID) where T : TRegistrable
        {
            return _registeredByID[registeredID] is T;
        }

        public T GetInstance<T>(TRegistrableID registeredID) where T : class, TRegistrable
        {
            return _registeredByID[registeredID] as T;
        }

        public bool ItemExists(TRegistrableID registeredID)
        {
            return _registeredByID.ContainsKey(registeredID);
        }

        public void Register(TRegistrable registeredItem, Action onRegistrationSucceed = null)
        {
            ResetData();
            if (registeredItem.referenceID == null || _registeredByID.ContainsKey(registeredItem.referenceID)) return;
            
            
            _registeredByID.Add(registeredItem.referenceID, registeredItem);
            _registered.Add(registeredItem);
            _count++;
                
            ReactToRegistration(registeredItem);

            onRegistrationSucceed?.Invoke();

        }

        protected virtual void ReactToRegistration(TRegistrable registeredItem)
        {
            
        }

        public void Unregister(TRegistrable registeredItem, Action onUnregistrationSucceed = null)
        {
            if (!_registeredByID.ContainsKey(registeredItem.referenceID)) return;
            
            
            ReactToUnregistration(registeredItem);
                
            if (registeredItem.referenceID != null)
                _registeredByID.Remove(registeredItem.referenceID);
                
            _registered.Remove(registeredItem);
            _count--;
                
            onUnregistrationSucceed?.Invoke();
        }

        protected virtual void ReactToUnregistration(TRegistrable registeredItem)
        {
            
        }
    }
}