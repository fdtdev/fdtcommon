﻿namespace com.FDT.Common
{
    public interface IRegistrable<TRegistrableID>:IRegistrableBase where TRegistrableID : IRegistrableID
    {
        TRegistrableID referenceID { get; }
    }
}