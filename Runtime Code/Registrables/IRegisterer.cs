﻿using System;

namespace com.FDT.Common
{
    public interface IRegisterer<TRegistrable, TRegistrableID>:IRegistererBase where TRegistrable : IRegistrable<TRegistrableID>
        where TRegistrableID : IRegistrableID
    {
        void Register(TRegistrable registeredItem, Action onRegistrationSucceed = null);
        void Unregister(TRegistrable registeredItem, Action onUnregistrationSucceed = null);
    }
}