using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common
{
    // --------------
    // Creation Date:   09/02/2020 17:39:47
    // Product Name:    FDT Common
    // Developers:      FDT Dev
    // Company:         FDT Dev
    // Description:     
    // --------------
    public abstract class RegisteredBase<TRegistrableId> : MonoBehaviour where TRegistrableId : IRegistrableID
    {
         #region Classes and Structs
         
         #endregion

         #region GameEvents and UnityEvents
         //[Header("GameEvents"), SerializeField] 
         //[Header("UnityEvents"), SerializeField] 
         #endregion

         #region Actions, Delegates and Funcs
         
         #endregion

         #region Inspector Fields
         [SerializeField] protected TRegistrableId _referenceID;
         //[Header("Public"), SerializeField] 
         //[Header("Protected"), SerializeField]
         //[Header("Private"), SerializeField]
         #endregion

         #region Properties, Consts and Statics
         protected virtual bool CanRegister => true;
         protected virtual bool CanUnregister => true;

         public TRegistrableId referenceID => _referenceID;

         protected virtual bool RegisterOnEnable => true;
         #endregion

         #region Methods
         protected virtual void HandleOnDisable()
         {
         }
         protected virtual void ReactToRegistration()
         {
         }
         protected virtual void ReactToUnregistration()
         {
         }

         protected void Awake()
         {
             if (!RegisterOnEnable && CanRegister)
             {
                 Register();
             }
             HandleAwake();
         }

         protected virtual void HandleAwake()
         {
             
         }
         protected virtual void HandleOnDestroy()
         {
             
         }
         protected void OnDestroy()
         {
             HandleOnDestroy();
             if (!RegisterOnEnable && CanUnregister)
             {
                 Unregister();
             }
         }

         protected void OnEnable()
         {
             if (RegisterOnEnable && CanRegister)
             {
                 Register();
             }
             HandleOnEnable();
         }

         protected virtual void HandleOnEnable()
         {
         }

         protected void OnDisable()
         {
             HandleOnDisable();
             if (RegisterOnEnable && CanUnregister)
             {
                 Unregister();
             }
         }
         protected abstract void Register();
         protected abstract void Unregister();

         #endregion
    }
}