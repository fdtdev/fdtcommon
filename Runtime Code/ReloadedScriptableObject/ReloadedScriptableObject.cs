﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common.ReloadedScriptableObject
{
    public class ReloadedScriptableObject : ScriptableObjectWithDescription
    {
#if UNITY_EDITOR
        public virtual bool ManualSerializationControls { get { return true; } }

        #region Methods
        public void SaveData()
        {
            var castType = this.GetType();
            var o = System.Convert.ChangeType(this, castType);
            serializedData = JsonUtility.ToJson(o);
        }
        public void LoadData()
        {
            var castType = this.GetType();
            var o = System.Convert.ChangeType(this, castType);
            JsonUtility.FromJsonOverwrite(serializedData, o);
        }
        #endregion
        
        [SerializeField, HideInInspector] protected string serializedData;
#endif
    }
}
