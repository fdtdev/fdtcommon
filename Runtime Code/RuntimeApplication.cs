﻿using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace com.FDT.Common
{
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public static class RuntimeApplication
    {
        public static bool isQuittingEditor = false;
        
#if UNITY_EDITOR
        public static PlayModeStateChange lastValue;

        public static Action OnExitPlayMode;
        public static Action OnEnterPlayMode;
        static RuntimeApplication()
        {
            isQuittingEditor = false;
            EditorApplication.playModeStateChanged += LogPlayModeState;
        }
        private static void LogPlayModeState(PlayModeStateChange state)
        {
            lastValue = state;
            if (state == PlayModeStateChange.EnteredPlayMode)
            {
                isQuittingEditor = false;
                OnEnterPlayMode?.Invoke();
            }

            if (state == PlayModeStateChange.ExitingPlayMode)
            {
                isQuittingEditor = true;
                OnExitPlayMode?.Invoke();
            }
        }
#endif

        public static bool isPlaying
        {
            get { return !isQuittingEditor && Application.isPlaying; }
        }
       
        public static void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_ANDROID
		AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
		activity.Call<bool>("moveTaskToBack" , true); 
#elif UNITY_IOS
		Application.Quit();
#endif
        }
    }

}
