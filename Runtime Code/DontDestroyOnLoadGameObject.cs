﻿using UnityEngine;

namespace com.FDT.Common
{
    public class DontDestroyOnLoadGameObject : MonoBehaviour
    {
        #region Methods
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        #endregion
    }
}