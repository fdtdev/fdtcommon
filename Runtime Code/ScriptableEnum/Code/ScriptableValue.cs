﻿using System;
using UnityEngine;

namespace com.FDT.Common.ScriptableEnum
{
    public class ScriptableValue : ScriptableObject,IEquatable<ScriptableEnum>,IEquatable<ScriptableValue>
    {
        public int enumValue;

        public bool Equals(ScriptableEnum other)
        {
            return enumValue == other.enumValue;
        }

        public bool Equals(ScriptableValue other)
        {
            if (other == null)
                return false;
            return enumValue == other.enumValue;
        }
        public override bool Equals(object obj){
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is ScriptableEnum)
                return Equals(obj as ScriptableEnum);
            if (obj is ScriptableValue)
                return Equals(obj as ScriptableValue);
            return false;
        }
        public bool Equals(UnityEngine.Object obj){
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is ScriptableValue)
                return Equals(obj as ScriptableValue);
            return false;
        }
        public static implicit operator int(ScriptableValue instance)
        {
            return instance.enumValue;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}