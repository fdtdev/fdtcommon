﻿using System;
using UnityEngine;

// Tested using interface for equals but it messes with the list creation in the drawer
namespace com.FDT.Common.ScriptableEnum
{
    [System.Serializable]
    public class ScriptableEnum:IEquatable<ScriptableEnum>,IEquatable<ScriptableValue>
    {
        public virtual Type GetEnumType()
        {
            return typeof(ScriptableValue);
        }

        public virtual string GetNoneLabel()
        {
            return "none";
        }

        [SerializeField] protected int _enumValue = 0;

        public int enumValue
        {
            get { return _enumValue;}
            set { _enumValue = value; }
        }
        public bool Equals(ScriptableEnum other)
        {
            return _enumValue == other.enumValue;
        }

        public bool Equals(ScriptableValue other)
        {
            if (other == null)
                return false;
            return _enumValue == other.enumValue;
        }
        public override bool Equals(object obj){
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is ScriptableEnum)
                return Equals(obj as ScriptableEnum);
            if (obj is ScriptableValue)
                return Equals(obj as ScriptableValue);
            return false;
        }
        public bool Equals(UnityEngine.Object obj){
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj is ScriptableValue)
                return Equals(obj as ScriptableValue);
            return false;
        }

        public static bool operator ==(ScriptableEnum x, ScriptableEnum y)
        {
            return x.Equals(y);
        }
        public static bool operator !=(ScriptableEnum x, ScriptableEnum y)
        {
            return !(x == y);
        }
        public static bool operator ==(ScriptableEnum x, ScriptableValue y)
        {
            return x.Equals(y);
        }
        public static bool operator !=(ScriptableEnum x, ScriptableValue y)
        {
            return !(x == y);
        }
        public static implicit operator int(ScriptableEnum instance)
        {
            return instance.enumValue;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}