﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.Common
{
    public static class Extensions
    {
        public static CoroutineHelper cHelper = null;

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source != null && toCheck != null && source.IndexOf(toCheck, comp) >= 0;
        }
        public static Coroutine StartCoroutine(System.Collections.IEnumerator routine)
        {
            if (cHelper == null)
            {
                GameObject o = new GameObject();
                o.name = "CoroutineHelper";
                GameObject.DontDestroyOnLoad(o);
                cHelper = o.AddComponent<CoroutineHelper>();
            }
            var result = cHelper.StartCoroutine(routine);
            return result;
        }
        public static Coroutine StartCoroutine(string methodName)
        {
            if (cHelper == null)
            {
                GameObject o = new GameObject();
                cHelper = o.AddComponent<CoroutineHelper>();
            }
            var result = cHelper.StartCoroutine(methodName);
            return result;
        }
        public static void StopCoroutine(Coroutine routine)
        {
            if (cHelper != null)
            {
                cHelper.StopCoroutine(routine);
            }
        }
        public static void StopCoroutine(string methodName)
        {
            if (cHelper != null)
            {
                cHelper.StopCoroutine(methodName);
            }
        }
        public static void StopCoroutine(System.Collections.IEnumerator routine)
        {
            if (cHelper != null)
            {
                cHelper.StopCoroutine(routine);
            }
        }

        public static void FillWith<T>(this List<T> target, List<T> source)
        {
            target.Clear();
            target.AddRange(source);
        }

        public static bool IsPrefab(this UnityEngine.Component target)
        {
            return string.IsNullOrEmpty(target.gameObject.scene.name);
        }
    }
    public class CoroutineHelper : MonoBehaviour
    {
    }
}