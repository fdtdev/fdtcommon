﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScriptableObject : ScriptableObject
{
    public virtual void ResetData()
    {
        
    }
}
