﻿using System.Collections;
using System.Collections.Generic;
using com.FDT.Common;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DontDestroyOnLoadGameObject))]
public class DontDestroyOnLoadGameObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EditorGUILayout.HelpBox("This GameObject will not be destroyed when loading scene", MessageType.Info);
        DrawDefaultInspector();
    }
}
