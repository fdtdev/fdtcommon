using System.Globalization;
using com.FDT.Common.RotorzReorderableList;
using UnityEditor;

namespace com.FDT.Common.Editor
{
    public class TemplateEditorWindow : EditorWindow
    {
        #region Classes and Structs

        #endregion

        #region GameEvents and UnityEvents

        //[Header("GameEvents"), SerializeField] 

        #endregion

        #region Actions, Delegates and Funcs

        #endregion

        #region Fields

        //[Header("References"), SerializeField] 

        #endregion

        #region Properties, Consts and Statics

        private static string namespaceText;
        private static string productName;
        private static string companyName;
        private static string devName;
        private static string scriptableMenu;
        
        #endregion

        #region Unity Methods
        
        #endregion

        #region Methods
        [MenuItem("Window/Template Options...", false, 15)]
        static void CreateWindow()
        {
            TemplateEditorWindow options = GetWindow<TemplateEditorWindow>(true, "Template Options", true);
            options.Show();
            Init();
        }
        static void Init()
        {
            namespaceText = TemplateCustomization.GetNamespaceText();
            productName = TemplateCustomization.GetProductName();
            companyName = TemplateCustomization.GetCompanyName();
            devName = TemplateCustomization.GetDevName();
            scriptableMenu = TemplateCustomization.GetScriptMenu();
        }

        void OnGUI()
        {
            ReorderableListGUI.Title("All Scripts");
            EditorGUI.BeginChangeCheck();
            namespaceText = EditorGUILayout.TextField("Namespace", namespaceText);
            FailReason namespaceError = namespaceText.ValidNamespace();
            if (EditorGUI.EndChangeCheck() && namespaceError == FailReason.NONE)
            {
                EditorPrefs.SetString(TemplateCustomization.NamespaceTextVar, namespaceText);
            }

            if (namespaceError != FailReason.NONE)
            {
                string failError = EditorExtensions.GetNamespaceErrorString(namespaceError);
                EditorGUILayout.HelpBox(failError, MessageType.Error);
            }
            
            EditorGUI.BeginChangeCheck();
            productName = EditorGUILayout.TextField("Product Name:", productName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.ProductNameVar, productName);
            }
            
            EditorGUI.BeginChangeCheck();
            companyName = EditorGUILayout.TextField("Company Name:", companyName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.CompanyNameVar, companyName);
            }
            
            EditorGUI.BeginChangeCheck();
            devName = EditorGUILayout.TextField("Developer Name:", devName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.DevNameVar, devName);
            }
            
            ReorderableListGUI.Title("ScriptableObjects");
            
            EditorGUI.BeginChangeCheck();
            scriptableMenu = EditorGUILayout.TextField("Creation menu path:", scriptableMenu);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.ScriptableMenuVar, scriptableMenu);
            }
        }
        #endregion
    }
}