﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

#region Header
/**
 *
 * original version available in https://github.com/anchan828/property-drawer-collection
 * 
**/
#endregion 
namespace com.FDT.Common.Editor
{
	[CustomPropertyDrawer(typeof(ButtonEnumAttribute))]
	public class ButtonEnumAttributeDrawer : CastedPropertyDrawer<ButtonEnumAttribute>
	{
		protected override List<SerializedPropertyType> validTypes { get { return new List<SerializedPropertyType> (new SerializedPropertyType[] { SerializedPropertyType.Enum }); } }
		public override void DoOnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position,label,property);
			if (property.enumValueIndex == -1)
				property.enumValueIndex = 0;
			if (GUI.Button (position, new GUIContent( property.enumDisplayNames [property.enumValueIndex], label.tooltip))) 
			{
				if (property.enumValueIndex < property.enumNames.Length - 1) {
					property.enumValueIndex++;
				} else {
					property.enumValueIndex = 0;
				}
			}
			EditorGUI.EndProperty();
		}
	}
}