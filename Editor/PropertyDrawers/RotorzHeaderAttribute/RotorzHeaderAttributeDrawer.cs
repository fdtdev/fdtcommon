﻿using UnityEngine;
using UnityEditor;
using com.FDT.Common.RotorzReorderableList;

namespace com.FDT.Common.Editor
{
    [CustomPropertyDrawer(typeof(RotorzHeaderAttribute))]
    public class RotorzHeaderAttributeDrawer : CastedDecoratorDrawer<RotorzHeaderAttribute>
    {
        private const int HeaderHeight = 20;
        private const int HeaderY = 12;
		
        public override void OnGUI(Rect position)
        {
            position.y += HeaderY;
            position.height = HeaderHeight;
            ReorderableListGUI.Title(position, cAttribute.headerText);
        }
		
        public override float GetHeight ()
        {
            return base.GetHeight() + HeaderHeight;
        }
    }
}