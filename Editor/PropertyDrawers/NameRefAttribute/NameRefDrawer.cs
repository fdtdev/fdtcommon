﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace com.FDT.Common.Editor
{
    [CustomPropertyDrawer(typeof(NameRefAttribute))]
    public class NameRefDrawer : CastedPropertyDrawer<NameRefAttribute> {

        protected override List<SerializedPropertyType> validTypes { get { return new List<SerializedPropertyType> (new SerializedPropertyType[] { SerializedPropertyType.String }); } }

        public override void DoOnGUI(Rect position, SerializedProperty prop, GUIContent label) {
			
            EditorGUI.BeginProperty (position, label, prop);

            Object o = null;
            if (!string.IsNullOrEmpty(prop.stringValue))
            {
                bool found = false;
                var a = AssetDatabase.FindAssets($"{prop.stringValue} t:{cAttribute.type.ToString()}");
                if (a.Length > 1)
                {
                    for (int i = 0; i < a.Length; i++)
                    {
                        var path = AssetDatabase.GUIDToAssetPath(a[i]);
                        var filename = Path.GetFileNameWithoutExtension(path);
                        if (filename == prop.stringValue)
                        {
                            found = true;
                            o = AssetDatabase.LoadAssetAtPath<Object>(path) as UnityEngine.Object;
                            break;
                        }
                    }
                    if (!found)
                    {
                        Debug.Log($"more than one {prop.stringValue} {cAttribute.type.ToString()} was found.");
                    }
                }
                else if (a.Length == 1)
                {
                    o = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(a[0])) as UnityEngine.Object;
                }
            }

            EditorGUI.BeginChangeCheck();
            o = EditorGUI.ObjectField(position, label, o, cAttribute.type, false);
            if (EditorGUI.EndChangeCheck())
            {
                if (o != null)
                {
                    prop.stringValue = o.name;
                }
                else
                {
                    prop.stringValue = null;
                }

                prop.serializedObject.ApplyModifiedProperties();
            }
            EditorGUI.EndProperty ();
        }
    }
}