﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
    public static class EditorExtensions
    {
        private static readonly string[] reserved = {
                "abstract",
                "as",
                "base",
                "bool",
                "break",
                "byte",
                "case",
                "catch",
                "char",
                "checked",
                "class",
                "const",
                "continue",
                "decimal",
                "default",
                "delegate",
                "do",
                "double",
                "else",
                "enum",
                "event",
                "explicit",
                "extern",
                "false",
                "finally",
                "fixed",
                "float",
                "for",
                "foreach",
                "goto",
                "if",
                "implicit",
                "in",
                "int",
                "interface",
                "internal",
                "is",
                "lock",
                "long",
                "namespace",
                "new",
                "null",
                "object",
                "operator",
                "out",
                "override",
                "params",
                "private",
                "protected",
                "public",
                "readonly",
                "ref",
                "return",
                "sbyte",
                "sealed",
                "short",
                "sizeof",
                "stackalloc",
                "static",
                "string",
                "struct",
                "switch",
                "this",
                "throw",
                "true",
                "try",
                "typeof",
                "uint",
                "ulong",
                "unchecked",
                "unsafe",
                "ushort",
                "using",
                "virtual",
                "void",
                "volatile",
                "while"
            };

        public static bool IsValidNamespace(this string nspace)
        {
            return nspace.ValidNamespace() != FailReason.NONE;
        }
        public static FailReason ValidNamespace(this string nspace)
        {
            if (nspace.Contains("..")) return FailReason.DOUBLE_DOT;
            if (nspace.Contains(" ")) return FailReason.SPACE;
            if (nspace.EndsWith(".")) return FailReason.ENDSWITH_DOT;
            if (nspace.StartsWith(".")) return FailReason.STARTSWITH_DOT;
            
            var s = nspace.ToLower(CultureInfo.InvariantCulture);

            for (int i = 0; i < reserved.Length; i++)
            {
                if (s.StartsWith(reserved[i] + "."))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
                if (s.Contains("." + reserved[i] + "."))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
                if (s.EndsWith("." + reserved[i]))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
            }
            return FailReason.NONE;
        }
        public static float GetHeight(this SerializedProperty p)
        {
            return EditorGUI.GetPropertyHeight(p);
        }
        public static Type GetTypeFromName(string propName)
        {
            var all = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in all)
            {
                var e = a.GetType(propName);
                if (e != null)
                    return e;
                var allTypes = a.GetTypes();
                foreach (var t in allTypes)
                {
                    if (t.Name == propName)
                    {
                        return t;
                    }
                }
            }
            return null;
        }
        public static Assembly GetAssemblyFor(string propType)
        {
            var all = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in all)
            {
                var e = a.GetType(propType);
                if (e != null)
                    return a;
                var allTypes = a.GetTypes();
                foreach (var t in allTypes)
                {
                    /*if (t.Name.Contains("Book"))
                    {
                        Debug.Log(t.Name + "  -------- " + a.GetName().Name);
                    }*/

                    if (t.Name == propType)
                    {
                        return a;
                    }
                }
                
            }
            return null;
        }
        public static T GetAssetFromSearch<T> (string search, string explicitClassName, string explicitObjectName) where T : UnityEngine.Object
        {
            var results = AssetDatabase.FindAssets(search);
            T icon = null;
            if (results.Length > 0)
            {
                for (int i = 0; i < results.Length; i++)
                {
                    var path = AssetDatabase.GUIDToAssetPath(results[i]);
                    var p = Path.GetFileNameWithoutExtension(path);
                    if (p == explicitObjectName)
                    {
                        icon = AssetDatabase.LoadAssetAtPath<T>(path);
                        if (icon != null)
                        {
                            return icon;
                        }
                    }
                }
            }
            return null;
        }
        
        public static object GetTargetObjectOfProperty(SerializedProperty prop)
        {
            if (prop == null) return null;

            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("["));
                    var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                    obj = GetValue_Imp(obj, elementName, index);
                }
                else
                {
                    obj = GetValue_Imp(obj, element);
                }
            }
            return obj;
        }
        private static object GetValue_Imp(object source, string name)
        {
            if (source == null)
                return null;
            var type = source.GetType();
            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                if (f != null)
                    return f.GetValue(source);
                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p != null)
                    return p.GetValue(source, null);
                type = type.BaseType;
            }
            return null;
        }
        private static object GetValue_Imp(object source, string name, int index)
        {
            var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
            if (enumerable == null) return null;
            var enm = enumerable.GetEnumerator();
            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext()) return null;
            }
            return enm.Current;
        }
        
        public static void SetIcon(string className, string explicitObjectName)
        {
            MonoScript script = EditorExtensions.GetAssetFromSearch<MonoScript>(string.Format("t:script {0}", className), className, className);
            MethodInfo SetIconForObject = typeof(EditorGUIUtility).GetMethod("SetIconForObject", BindingFlags.Static | BindingFlags.NonPublic);
            MethodInfo CopyMonoScriptIconToImporters = typeof(MonoImporter).GetMethod("CopyMonoScriptIconToImporters", BindingFlags.Static | BindingFlags.NonPublic);
            Texture2D icon = GetAssetFromSearch<Texture2D>("t:Texture2D " + explicitObjectName, "Texture2D", explicitObjectName);
            SetIconForObject.Invoke(null, new object[]{ script, icon });
            CopyMonoScriptIconToImporters.Invoke(null, new object[]{ script });
        }

        public static string GetNamespaceErrorString(FailReason namespaceError)
        {
            switch (namespaceError)
            {
                case FailReason.SPACE:
                    return "Invalid namespace: Space character detected.";
                case FailReason.UNKNOWN:
                    return "Invalid namespace: Unknown reason.";
                case FailReason.DOUBLE_DOT:
                    return "Invalid namespace: Double dot detected.";
                case FailReason.ENDSWITH_DOT:
                    return "Invalid namespace: Ends with a dot.";
                case FailReason.STARTSWITH_DOT:
                    return "Invalid namespace: Starts with a dot.";
                case FailReason.CONTAINS_RESERVED:
                    return "Invalid namespace: Contains reserved word.";
            }
            return "Invalid namespace: Unknown error.";
        }

        public static void CreateScriptAsset(string templatePath, string destName) 
        {
        #if UNITY_2019_1_OR_NEWER
            UnityEditor.ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, destName);
        #else
	        typeof(UnityEditor.ProjectWindowUtil)
		        .GetMethod("CreateScriptAsset", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic)
		        .Invoke(null, new object[] { templatePath, destName });
        #endif
        }
    }
}