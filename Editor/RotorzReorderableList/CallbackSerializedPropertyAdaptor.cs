﻿using System.Collections;
using System.Collections.Generic;
using com.FDT.Common.RotorzReorderableList;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.RotorzReorderableList
{
    
    public class CallbackSerializedPropertyAdaptor : SerializedPropertyAdaptor
    {
        public System.Action<Rect, int> OnBeforeDrawItemBackground;
        public System.Action<Rect, int> OnAfterDrawItemBackground;
        
        public System.Action<Rect, int> OnBeforeDrawItem;
        public System.Action<Rect, int> OnAfterDrawItem;

        
        public CallbackSerializedPropertyAdaptor(SerializedProperty arrayProperty, float fixedItemHeight) : base(arrayProperty, fixedItemHeight)
        {
        }

        public CallbackSerializedPropertyAdaptor(SerializedProperty arrayProperty) : base(arrayProperty)
        {
        }

        public override void DrawItemBackground(Rect position, int index)
        {
            OnBeforeDrawItemBackground?.Invoke(position, index);
            base.DrawItemBackground(position, index);
            OnAfterDrawItemBackground?.Invoke(position, index);
        }

        public override void DrawItem(Rect position, int index)
        {
            OnBeforeDrawItem?.Invoke(position, index);
            base.DrawItem(position, index);
            OnAfterDrawItem?.Invoke(position, index);
        }
    }
}