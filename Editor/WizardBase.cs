using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
// --------------
// Creation Date:   02/02/2020 20:58:54
// Product Name:    FDT Common
// Developers:      FDT Dev
// Company:         FDT Dev
// Description:     
// --------------
    public class WizardBase : EditorWindow
    {
        #region Classes and Structs
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Methods
        protected virtual int AskVariable(UsableTypeSelection paramData, UsableTypes uTypes)
        {
            EditorGUILayout.BeginHorizontal();

            int selectedIndex = paramData.Draw(uTypes);
            
            EditorGUILayout.LabelField( "PropName: ", GUILayout.Width(70));
            paramData.paramName = EditorGUILayout.TextField(GUIContent.none, paramData.paramName, GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            
            EditorGUILayout.EndHorizontal();
            string nspace = string.Empty;
            if (paramData.value != null)
	            nspace = paramData.value.additionalNamespace;
	        EditorGUILayout.LabelField($"Namespace: {nspace}");

            return selectedIndex;
        }
        protected string GetCurrentPath()
        {
            var obj = Selection.activeObject;
            if (obj != null)
            {
                var path = AssetDatabase.GetAssetPath (obj.GetInstanceID ());
                if (path.Length > 0) 
                {
                    if (Directory.Exists (path)) 
                    {
                        return path;

                    }
                }
            }
            return "Assets";
        }
        #endregion
    }
}