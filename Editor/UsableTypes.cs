﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace com.FDT.Common.Editor
{
    [System.Serializable]
    public class UsableTypes
    {
        public string[] GetDistinctAdditionalNamespaces(UsableType[] usableTypes)
        {
            if (usableTypes == null || usableTypes.Length == 0)
                return new string[0];

            string[] namespaceArray = new string[usableTypes.Length];
            for (int i = 0; i < namespaceArray.Length; i++)
            {
                namespaceArray[i] = usableTypes[i].additionalNamespace;
            }
            return namespaceArray.Distinct().ToArray();
        }
        public class Variable : IComparable
        {
            public string name;
            public UsableType usableType;

            int m_TypeIndex;

            public string NameAsPrivate
            {
                get
                {
                    string returnVal = "m_" + name[0].ToString().ToUpper();
                    for (int i = 1; i < name.Length; i++)
                    {
                        returnVal += name[i];
                    }
                    return returnVal;
                }
            }

            public Variable(string name, UsableType usableType)
            {
                this.name = name;
                this.usableType = usableType;
            }

            public bool GUI(UsableType[] usableTypes)
            {
                bool removeThis = false;
                EditorGUILayout.BeginHorizontal();
                name = EditorGUILayout.TextField(name);
                m_TypeIndex = EditorGUILayout.Popup(m_TypeIndex, GetNamewithSortingArray(usableTypes));
                usableType = usableTypes[m_TypeIndex];
                if (GUILayout.Button("Remove", GUILayout.Width(60f)))
                {
                    removeThis = true;
                }
                EditorGUILayout.EndHorizontal();

                return removeThis;
            }
            public string[] GetNamewithSortingArray(UsableType[] usableTypes)
            {
                if (usableTypes == null || usableTypes.Length == 0)
                    return new string[0];

                string[] displayNames = new string[usableTypes.Length];
                for (int i = 0; i < displayNames.Length; i++)
                {
                    displayNames[i] = usableTypes[i].nameWithSorting;
                }
                return displayNames;
            }
            public int CompareTo(object obj)
            {
                if (obj == null)
                    return 1;

                UsableType other = (UsableType)obj;

                if (other == null)
                    throw new ArgumentException("This object is not a Variable.");

                return String.Compare(name.ToLower(), other.name.ToLower(), StringComparison.Ordinal);
            }

            public UsableType[] GetUsableTypesFromVariableArray(Variable[] variables)
            {
                UsableType[] usableTypes = new UsableType[variables.Length];
                for (int i = 0; i < usableTypes.Length; i++)
                {
                    usableTypes[i] = variables[i].usableType;
                }
                return usableTypes;
            }
        }

        public class UsableType : IComparable
        {
            public readonly string name;
            public readonly string nameWithSorting;
            public readonly string additionalNamespace;
            public readonly GUIContent guiContent;
            public readonly GUIContent guiContentWithSorting;
            public readonly Type type;

            public readonly string[] unrequiredNamespaces =
            {
                "UnityEngine",
                "UnityEngine.Timeline",
                "UnityEngine.Playables"
            };
            public const string blankAdditionalNamespace = "";

            const string k_NameForNullType = "None";

            public UsableType(Type usableType)
            {
                type = usableType;

                if (type != null)
                {
                    name = usableType.DeclaringType!=null?usableType.DeclaringType.Name + "." + usableType.Name:usableType.Name;
                    nameWithSorting = name.ToUpper()[0] + "/" + name;
                    additionalNamespace = unrequiredNamespaces.All(t => usableType.Namespace != t) ? usableType.Namespace : blankAdditionalNamespace;
                }
                else
                {
                    name = k_NameForNullType;
                    nameWithSorting = k_NameForNullType;
                    additionalNamespace = blankAdditionalNamespace;
                }
                if (string.IsNullOrEmpty(additionalNamespace))
                {
                    guiContent = new GUIContent(name);
                    guiContentWithSorting = new GUIContent(nameWithSorting);
                }
                else
                {
                    guiContent = new GUIContent($"{name} ({additionalNamespace})");
                    guiContentWithSorting = new GUIContent($"{nameWithSorting} ({additionalNamespace})");
                }
            }

            public UsableType(string name)
            {
                this.name = name;
                nameWithSorting = name.ToUpper()[0] + "/" + name;
                additionalNamespace = blankAdditionalNamespace;
                if (string.IsNullOrEmpty(additionalNamespace))
                {
                    guiContent = new GUIContent(name);
                    guiContentWithSorting = new GUIContent(nameWithSorting);
                }
                else
                {
                    guiContent = new GUIContent($"{name} ({additionalNamespace})");
                    guiContentWithSorting = new GUIContent($"{nameWithSorting} ({additionalNamespace})");
                }
            }

            public int CompareTo(object obj)
            {
                if (obj == null)
                    return 1;

                UsableType other = (UsableType)obj;

                if (other == null)
                    throw new ArgumentException("This object is not a UsableType.");

                return String.Compare(name.ToLower(), other.name.ToLower(), StringComparison.Ordinal);
            }

            public GUIContent[] GetGUIContentWithSortingArray(UsableType[] usableTypes)
            {
                if (usableTypes == null || usableTypes.Length == 0)
                    return new GUIContent[0];

                GUIContent[] guiContents = new GUIContent[usableTypes.Length];
                for (int i = 0; i < guiContents.Length; i++)
                {
                    guiContents[i] = usableTypes[i].guiContentWithSorting;
                }
                return guiContents;
            }
        }
        public List<UsableType> usableTypes = new List<UsableType>();

        public void Init<T>()
        {
            #if UNITY_2019_2_OR_NEWER
            var typesUnity = TypeCache.GetTypesDerivedFrom<object>().ToList();
            var types = TypeCache.GetTypesDerivedFrom<UnityEngine.Object>().ToList();
            types.AddRange(typesUnity);
            Type[] robjectTypes = types.Where(t => typeof(T).IsAssignableFrom(t))
                .Where(t2 => !t2.IsAbstract)
                .ToArray();
            #else
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());
            Type[] robjectTypes = types.Where(t => typeof(object).IsAssignableFrom(t))
                .Where(t2 => !t2.IsAbstract)
                .ToArray();
            #endif
            
            UsableType gameObjectUsableType = new UsableType(typeof(GameObject));
            UsableType[] defaultUsableTypes = GetUsableTypeArray(robjectTypes, gameObjectUsableType);

            List<UsableType> exposedRefTypeList = defaultUsableTypes.ToList();
            exposedRefTypeList.Sort();
            var s_ExposedReferenceTypes = exposedRefTypeList.ToArray();
            var s_BehaviourVariableTypes = AmalgamateUsableTypes
                (
                    s_ExposedReferenceTypes,
                    new UsableType("int"),
                    new UsableType("bool"),
                    new UsableType("float"),
                    new UsableType("Color"),
                    new UsableType("double"),
                    new UsableType("string"),
                    new UsableType("Vector2"),
                    new UsableType("Vector3"),
                    new UsableType("AudioClip"),
                    new UsableType("Quaternion"),
                    new UsableType("AnimationCurve")
                );
            List<UsableType> scriptVariableTypeList = s_BehaviourVariableTypes.ToList();
            scriptVariableTypeList.Sort();
            usableTypes = scriptVariableTypeList;
        }
        public UsableType[] GetUsableTypeArray(Type[] types, params UsableType[] additionalUsableTypes)
        {
            List<UsableType> usableTypeList = new List<UsableType>();
            for (int i = 0; i < types.Length; i++)
            {
                usableTypeList.Add(new UsableType(types[i]));
            }
            usableTypeList.AddRange(additionalUsableTypes);
            return usableTypeList.ToArray();
        }
        public UsableType[] AmalgamateUsableTypes(UsableType[] usableTypeArray, params UsableType[] usableTypes)
        {
            List<UsableType> usableTypeList = new List<UsableType>();
            for (int i = 0; i < usableTypes.Length; i++)
            {
                usableTypeList.Add(usableTypes[i]);
            }
            usableTypeList.AddRange(usableTypeArray);
            return usableTypeList.ToArray();
        }
    }
}