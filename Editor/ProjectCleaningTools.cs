﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace com.FDT.Common.Editor
{
// Removes empty folders
    public class ProjectCleaningTools : MonoBehaviour
    {
        
        [MenuItem("Project Tools/Cleaning/Remove empty folders")]
        public static void RemoveEmptyFolders()
        {
            string rootPath = Application.dataPath;

            int removedDirectories = 0;
            int pass = 0;
            do
            {
                removedDirectories = RemoveEmptyFoldersRecursive(rootPath);
                Debug.Log(string.Format("Removed directories on pass {0}: {1}", pass, removedDirectories));
                pass++;
            } while (removedDirectories > 0);
        }


        private static int RemoveEmptyFoldersRecursive(string rootPath)
        {
            var removedDirectories = 0;

            foreach (var directory in Directory.GetDirectories(rootPath))
            {
                removedDirectories += RemoveEmptyFoldersRecursive(directory);

                bool isEmpty = Directory.GetFiles(directory).Length == 0 &&
                               Directory.GetDirectories(directory).Length == 0;
                isEmpty = isEmpty || HasOnlyMetaFiles(directory);

                if (isEmpty)
                {
                    Debug.Log("---> Removing dir: " + directory);
                    Directory.Delete(directory, true);
                    File.Delete(directory.ToString() + ".meta");
                    removedDirectories++;
                }
            }

            return removedDirectories;
        }


        private static bool HasOnlyMetaFiles(string path)
        {
            bool result = Directory.GetDirectories(path).Length == 0;
            if (result)
            {
                string[] files = Directory.GetFiles(path);
                int metaFiles = 0;
                int dsStoreFiles = 0;
                foreach (string file in files)
                {
                    metaFiles += (file.EndsWith(".meta")) ? 1 : 0;
                    dsStoreFiles += (file.EndsWith(".DS_Store")) ? 1 : 0;
                }

                result = ((metaFiles + dsStoreFiles) == files.Length);
            }

            if (result)
            {
                Debug.Log(string.Format("Directory with only metas: {0}", path));
            }

            return result;
        }
    }
}