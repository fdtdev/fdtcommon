﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
    [CustomEditor(typeof(ScriptableObjectWithDescription), true)]
    public class ScriptableObjectWithDescriptionEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();
            ScriptableObjectWithDescription cTarget = target as ScriptableObjectWithDescription;

            if (cTarget.HasCustomIcon && GUILayout.Button("Fix Icon"))
            {
                string scriptName = target.GetType().Name;
                string iconFileName = cTarget.IconFileName;
                EditorExtensions.SetIcon(scriptName, iconFileName);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}