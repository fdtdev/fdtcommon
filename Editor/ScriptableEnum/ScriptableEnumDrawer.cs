﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using com.FDT.Common.Editor;

namespace com.FDT.Common.ScriptableEnum.Editor
{
    [CustomPropertyDrawer(typeof(ScriptableEnum), true)]
    public class ScriptableEnumDrawer : PropertyDrawer
    {
        public static Dictionary<string, string> typeCache = new Dictionary<string, string>();
        protected List<ScriptableValue> values = new List<ScriptableValue>();
        protected string[] stringValues;
        protected Dictionary<int, ScriptableValue> intValues = new Dictionary<int, ScriptableValue>();
        protected string noneLabel = null;
        protected System.Type enumType;
        
        
        private int selected;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            if (values.Count == 0)
            {
                CacheValues(property);
            }

            SerializedProperty c = property.FindPropertyRelative("_enumValue");

            if (intValues.ContainsKey(c.intValue))
            {
                selected = values.IndexOf(intValues[c.intValue]);    
            }
            else
            {
                selected = 0;
            }
            

            EditorGUI.BeginChangeCheck();
            selected = EditorGUI.Popup(position, label.text, selected, stringValues);
            if (EditorGUI.EndChangeCheck())
            {
                if (values[selected] != null)
                {
                    c.intValue = values[selected].enumValue;    
                }
                else
                {
                    c.intValue = 0;
                }
                c.serializedObject.ApplyModifiedProperties();
            }

            EditorGUI.EndProperty();
        }

        private void CacheValues(SerializedProperty property)
        {
            string propType = property.type;
            string result = null;
            object activator = null;
            MethodInfo method = null;
            
            if (typeCache.ContainsKey(propType))
            {
                result = typeCache[propType];
            }
            else
            {
                if (enumType == null)
                {
                    enumType = EditorExtensions.GetTypeFromName(propType);
                }

                activator = Activator.CreateInstance(enumType);
                method = enumType.GetMethod("GetEnumType");
                result = (method.Invoke(activator, null) as Type).Name;
                typeCache.Add(propType, result);
                
            }

            if (string.IsNullOrEmpty(noneLabel))
            {
                if (activator == null)
                {
                    if (enumType == null)
                    {
                        enumType = EditorExtensions.GetTypeFromName(propType);
                    }
                    activator = Activator.CreateInstance(enumType);
                }
                method = enumType.GetMethod("GetNoneLabel");
                noneLabel = method.Invoke(activator, null).ToString();
            }
            var e = AssetDatabase.FindAssets("t:" + result);
            if (e.Length > 0)
            {
                for (int i = 0; i < e.Length; i++)
                {
                    var o =
                        AssetDatabase.LoadAssetAtPath<ScriptableValue>(AssetDatabase.GUIDToAssetPath(e[i])) as
                            ScriptableValue;
                    if (o != null && !values.Contains(o))
                    {
                        values.Add(o);
                        intValues.Add(o.enumValue, o);
                    }
                }
            }
            values = values.OrderBy(o => o.enumValue).ToList();
            List<string> str = new List<string>();
            if (values.Count > 0)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    str.Add(string.Format("{0}({1})", values[i].name, values[i].enumValue.ToString()));
                }
            }

            if (!intValues.ContainsKey(0))
            {
                str.Insert(0,noneLabel);
                values.Insert(0, null);
            }
            stringValues = str.ToArray();
        }
    }
}