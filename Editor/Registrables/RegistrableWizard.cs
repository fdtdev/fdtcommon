using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
// --------------
// Creation Date:   01/02/2020 19:37:18
// Product Name:    FDT Common
// Developers:      FDT Dev
// Company:         FDT Dev
// Description:     
// --------------
    public class RegistrableWizard : WizardBase
    {
        #region Classes and Structs

        public enum RegistererType
        {
            SCRIPTABLE = 0, SINGLETON = 1
        }

        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        static UsableTypes registrableIDTypes = new UsableTypes();
        static UsableTypes registrableTypes = new UsableTypes();
        static UsableTypes registererTypes = new UsableTypes();
        
        private static string registrableName;
        private static RegistererType registererType;

        public static string registrableWizardNamespace = "com.FDT.Common";
        
        private static bool createRegistrableID = true; // create or select from existing
        private static bool createRegistrable = true; // create or select from existing
        private static bool createRegisterer = true; // create or select from existing
        private static UsableTypeSelection dataRegistrableID = new UsableTypeSelection();
        private static UsableTypeSelection dataRegistrable = new UsableTypeSelection();
        private static UsableTypeSelection dataRegisterer = new UsableTypeSelection();
        private static bool registerOnEnable = true;
        private static string path;
        
        #endregion

        #region Methods

        //TODO: enable menu item when ready
        [MenuItem("Window/Registrable Wizard...")]
        static void CreateWindow ()
        {
            RegistrableWizard wizard = GetWindow<RegistrableWizard>(true, "Registrable Wizard", true);

            Vector2 position = Vector2.zero;
            SceneView sceneView = SceneView.lastActiveSceneView;
            if (sceneView != null)
                position = new Vector2(sceneView.position.x, sceneView.position.y);
            wizard.Show();

            Init ();
        }
        
        static void Init ()
        {
            if (registrableTypes.usableTypes.Count == 0)
                registrableTypes.Init<IRegistrableBase> ();
            if (registererTypes.usableTypes.Count == 0)
                registererTypes.Init<IRegistererBase> ();
            if (registrableIDTypes.usableTypes.Count == 0)
                registrableIDTypes.Init<IRegistrableID> ();
            
        }
        void OnGUI ()
		{
			if (registrableTypes.usableTypes.Count == 0 || registererTypes.usableTypes.Count == 0 || registrableIDTypes.usableTypes.Count == 0)
				Init ();

			EditorGUILayout.HelpBox ("This wizard is used to create a Registrable.\nIt will create 2 or 3 ready to use scripts, depending on your settings.\nYou shouldn't need anything else.", MessageType.None);
			EditorGUILayout.Space ();
            registerOnEnable = EditorGUILayout.Toggle("Register methods on Enable/Disable ?", registerOnEnable);
            registrableWizardNamespace = TemplateCustomization.GetNamespaceText();
            EditorGUI.BeginChangeCheck ();
            registrableWizardNamespace = EditorGUILayout.TextField ("Namespace ", registrableWizardNamespace);
            FailReason failReason = registrableWizardNamespace.ValidNamespace();
            if (failReason != FailReason.NONE)
            {
                string error = EditorExtensions.GetNamespaceErrorString(failReason);
                EditorGUILayout.HelpBox(error, MessageType.Error);
            }
            
            
            if (EditorGUI.EndChangeCheck () && failReason == FailReason.NONE) {
                EditorPrefs.SetString (TemplateCustomization.NamespaceTextVar, registrableWizardNamespace);
            }

            registrableName = EditorGUILayout.TextField("Registrable base name: " , registrableName);
            
            // registrableID
            createRegistrableID = EditorGUILayout.Toggle("Create Registrable ID ? ", createRegistrableID);
            if (createRegistrableID)
            {
                //registrableIDClassName = EditorGUILayout.TextField("RegistrableID Class name: ", registrableIDClassName);
            }
            else
            {
                AskVariable(dataRegistrableID, registrableIDTypes);
            }
            GUILayout.Space(25);
            // registrable
            createRegistrable = EditorGUILayout.Toggle("Create Registrable ? ", createRegistrable);
            if (!createRegistrable)
            {
                AskVariable(dataRegistrable, registrableTypes);
            }
            GUILayout.Space(25);
            //registerer
            createRegisterer = EditorGUILayout.Toggle("Create Registerer ? ", createRegisterer);
            if (createRegisterer)
            {
                registererType = (RegistererType) EditorGUILayout.EnumPopup("Registerer Type: ", registererType);
                //registererClassName = EditorGUILayout.TextField("Registerer Class name: ", registererClassName);
            }
            else
            {
                AskVariable(dataRegisterer, registererTypes);
            }
            GUILayout.Space(25);
            if (createRegistrableID || createRegistrable || createRegisterer)
            {
                if (GUILayout.Button("Create Registrable"))
                {
                    CreateRegistrable();
                }
            }
        }

        protected class ClassData
        {
            public string className;
            public string classNamespace;
        }
        protected void CreateRegistrable()
        {
            path = GetCurrentPath();
            // registrableID
            ClassData registrableIDData = new ClassData();
            if (!createRegistrableID)
            {
                registrableIDData.className = dataRegistrableID.value.name;
                registrableIDData.classNamespace = dataRegistrableID.value.additionalNamespace;
            }
            else
            {
                //registrableIDData.className = registrableIDClassName;
                registrableIDData.className = registrableName + "RegistrableIDAsset";
                registrableIDData.classNamespace = registrableWizardNamespace;
            }
            Debug.Log($"{registrableIDData.classNamespace} - {registrableIDData.className}");

            // registrable
            ClassData registrableData = new ClassData();
            if (!createRegistrable)
            {
                registrableData.className = dataRegistrable.value.name;
                registrableData.classNamespace = dataRegistrable.value.additionalNamespace;
            }
            else
            {
                //registrableData.className = registrableClassName;
                registrableData.className = registrableName + "Registrable";
                registrableData.classNamespace = registrableWizardNamespace;
            }
            Debug.Log($"{registrableData.classNamespace} - {registrableData.className}");
            
            // registerer
            ClassData registererData = new ClassData();
            
            if (!createRegisterer)
            {
                registererData.className = dataRegisterer.value.name;
                registererData.classNamespace = dataRegisterer.value.additionalNamespace;
            }
            else
            {
                //registererData.className = registererClassName;
                registererData.className = registrableName + "Registerer";
                registererData.classNamespace = registrableWizardNamespace;
            }
            Debug.Log($"{registererData.classNamespace} - {registererData.className}");

            if (createRegistrableID)
            {
                HandleCreateRegistrableID(registrableIDData, registrableData, registererData);
            }
            if (createRegistrable)
            {
                HandleCreateRegistrable(registrableIDData, registrableData, registererData);
            }
            if (createRegisterer)
            {
                HandleCreateRegisterer(registrableIDData, registrableData, registererData);
            }
            AssetDatabase.SaveAssets ();
            AssetDatabase.Refresh ();
        }

        private void HandleCreateRegisterer(ClassData registrableIdData, ClassData registrableData, ClassData registererData)
        {
            string filecontent = GetRegistererText(registrableData, registererData);
            
            
            filecontent = TemplateCustomization.BasicFillData(registererData.className, filecontent);
            filecontent = TemplateCustomization.FillData(registererData.className, filecontent);
            filecontent = filecontent.Replace("#RegistrableID#", registrableIdData.className);
            filecontent = filecontent.Replace("#Registrable#", registrableData.className);
            filecontent = AddNamespaces(filecontent, registrableIdData, registrableData, registererData);

            Debug.Log(filecontent);
            CreateScript(path, registererData.className, filecontent);
        }

        private string AddNamespaces(string result, ClassData registrableIdData, ClassData registrableData, ClassData registererData)
        {
            List<string> namespaces = new List<string>();
            namespaces.Add(registrableWizardNamespace);
            
            if (!string.IsNullOrEmpty(registrableIdData.classNamespace) &&
                !namespaces.Contains(registrableIdData.classNamespace))
            {
                namespaces.Add(registrableIdData.classNamespace);
            }
            if (!string.IsNullOrEmpty(registrableData.classNamespace) &&
                !namespaces.Contains(registrableData.classNamespace))
            {
                namespaces.Add(registrableData.classNamespace);
            }
            if (!string.IsNullOrEmpty(registererData.classNamespace) &&
                !namespaces.Contains(registererData.classNamespace))
            {
                namespaces.Add(registererData.classNamespace);
            }
            namespaces.RemoveAt(0); // removes registrableWizardNamespace
            
            foreach (var n in namespaces)
            {
                result = $"using {n};\n" + result;
            }

            return result;
        }

        private string GetRegistererText(ClassData registrableData, ClassData registererData)
        {
            string filecontent = string.Empty;
            string guid = string.Empty;
            if (registererType == RegistererType.SINGLETON)
            {
                guid = AssetDatabase.FindAssets("SingletonRegistererTemplate.cs t:TextAsset")[0];
            }
            else if (registererType == RegistererType.SCRIPTABLE)
            {
                guid = AssetDatabase.FindAssets("ScriptableObjectRegistererTemplate.cs t:TextAsset")[0];
            }
            
            var c = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(guid));
            filecontent = c.text;
            return filecontent;
        }

        private void HandleCreateRegistrableID(ClassData registrableIdData, ClassData registrableData, ClassData registererData)
        {
            string filecontent = string.Empty;
            var g = AssetDatabase.FindAssets("RegistrableIDAssetTemplate.cs t:TextAsset");
            if (g.Length > 0)
            {
                var c = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(g[0]));
                filecontent = c.text;
                filecontent = TemplateCustomization.BasicFillData(registrableIdData.className, filecontent);
                filecontent = TemplateCustomization.FillData(registrableIdData.className, filecontent);
                filecontent = AddNamespaces(filecontent, registrableIdData, registrableData, registererData);
                Debug.Log(filecontent);
                CreateScript(path, registrableIdData.className, filecontent);
            }
        }
        private void HandleCreateRegistrable(ClassData registrableIdData, ClassData registrableData, ClassData registererData)
        {
            string filecontent = GetRegistrableText(registrableData, registererData);
            
            filecontent = TemplateCustomization.BasicFillData(registrableData.className, filecontent);
            filecontent = TemplateCustomization.FillData(registrableData.className, filecontent);
            filecontent = filecontent.Replace("#RegistrableID#", registrableIdData.className);
            filecontent = filecontent.Replace("#Registerer#", registererData.className);
            filecontent = filecontent.Replace("#REGISTERONENABLE#", registerOnEnable?"true":"false");
            filecontent = AddNamespaces(filecontent, registrableIdData, registrableData, registererData);
            Debug.Log(filecontent);
            CreateScript(path, registrableData.className, filecontent);
        }

        private string GetRegistrableText(ClassData registrableData, ClassData registererData)
        {
            string filecontent = string.Empty;
            string guid = string.Empty;
            if (registererType == RegistererType.SINGLETON)
            {
                guid = AssetDatabase.FindAssets("RegisteredToSingletonTemplate.cs t:TextAsset")[0];
            }
            else if (registererType == RegistererType.SCRIPTABLE)
            {
                guid = AssetDatabase.FindAssets("RegisteredToScriptableObjectTemplate.cs t:TextAsset")[0];
            }
            
            var c = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(guid));
            filecontent = c.text;
            return filecontent;
        }

        protected override int AskVariable(UsableTypeSelection paramData, UsableTypes uTypes)
        {
            EditorGUILayout.BeginHorizontal();

            int selectedIndex = paramData.Draw(uTypes);
            
            EditorGUILayout.EndHorizontal();
            string nspace = string.Empty;
            if (paramData.value != null)
                nspace = paramData.value.additionalNamespace;
            EditorGUILayout.LabelField($"Namespace: {nspace}");

            return selectedIndex;
        }
        void CreateScript (string folderpath, string fileName, string content)
        {
            string path = folderpath + "/" + fileName + ".cs";
            using (StreamWriter writer = File.CreateText (path))
                writer.Write (content);
        }
        #endregion
    }
}