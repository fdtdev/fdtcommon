using UnityEngine;
using com.FDT.Common;

namespace #NAMESPACE#
{
    // --------------
    // Creation Date:   #CREATIONDATE#
    // Product Name:    #PROJECTNAME#
    // Developers:      #DEV#
    // Company:         #COMPANY#
    // Description:     #NOTRIM#
    // --------------
    [CreateAssetMenu(menuName = "#SCRIPTMENU#", fileName = "#SCRIPTABLEFILE#")]
    public class #SCRIPTNAME# : ScriptableObjectRegisterer<#Registrable#, #RegistrableID#>
    {
        #region Classes and Structs
        #NOTRIM#
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        #NOTRIM#
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        #NOTRIM#
        #endregion

        #region Methods
        #NOTRIM#        
        #endregion
    }
}