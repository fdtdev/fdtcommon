﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Adds the given define symbols to PlayerSettings define symbols.
    /// Just add your own define symbols to the Symbols property at the below.
    /// </summary>
    [InitializeOnLoad]
    public class EditorInit : UnityEditor.Editor
    {

        /// <summary>
        /// Symbols that will be added to the editor
        /// </summary>

        /// <summary>
        /// Add define symbols as soon as Unity gets done compiling.
        /// </summary>
        static EditorInit()
        {
            FindPackage("FDTInitializationHandler t:asmdef", "INITHANDLER");
            FindPackage("FDTGenericDataSaving t:asmdef", "GENERICDATASAVING");
            FindPackage("FDTStateMachineGraph t:asmdef", "SMGRAPH");
        }

        private static void FindPackage(string asmdef, string param)
        {
            string definesString =
                PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            List<string> allDefines = definesString.Split(';').ToList();

            var o = AssetDatabase.FindAssets(asmdef);
            bool mod = false;
            if (o.Length > 0 && !allDefines.Contains(param))
            {
                allDefines.Add(param);
                mod = true;
            }
            else if (o.Length == 0 && allDefines.Contains(param))
            {
                allDefines.Remove(param);
                mod = true;
            }

            if (mod)
            {
                PlayerSettings.SetScriptingDefineSymbolsForGroup(
                    EditorUserBuildSettings.selectedBuildTargetGroup,
                    string.Join(";", allDefines.ToArray()));
            }
        }
    }
}
