using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.Editor
{
// --------------
// Creation Date:   01/02/2020 20:26:18
// Product Name:    FDT Common
// Developers:      FDT Dev
// Company:         FDT Dev
// Description:     
// --------------
    public class UsableTypeSelection
    {
        #region Classes and Structs
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("Public"), SerializeField] 
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]
        #endregion

        #region Properties, Consts and Statics
        public string currText = string.Empty;
        public UsableTypes.UsableType value = null;
        public List<UsableTypes.UsableType> dropdownList = new List<UsableTypes.UsableType>();
        public int enumValueIndex = -1;
        public string paramName;
        public string paramTypeName;
        
        #endregion

        #region Methods
                

        protected List<EnumStringValuePair> cachedList = new List<EnumStringValuePair>();
        protected bool changed = true;
        
        public List<EnumStringValuePair> GetEnumList(UsableTypes source)
        {
            if (!changed)
                return cachedList;
            
            changed = false;
            List<EnumStringValuePair> result = new List<EnumStringValuePair>();
            if (string.IsNullOrEmpty(currText))
            {
                return result;
            }
            List<EnumStringValuePair> allList = new List<EnumStringValuePair>();
            var enumValues = source.usableTypes;
          
            for (int i = 0; i < enumValues.Count; ++i)
            {
                EnumStringValuePair pair = new EnumStringValuePair();
                pair.strValue = enumValues [i];
                pair.intValue = i;

                allList.Add(pair);
            }

            for (int i = 0; i < allList.Count; ++i)
            {
                if (allList[i].strValue.name.StartsWith(currText, StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Add(allList[i]);
                    if (result.Count > 50)    // fixes error : "Failed to insert item. Menu item insert limit is reached" when more than 1000.
                        break;
                }
            }

            cachedList = result;
            return result;
        }

        public struct EnumStringValuePair : IComparable<EnumStringValuePair>
        {
            public UsableTypes.UsableType strValue;
            public int intValue;

            public int CompareTo(EnumStringValuePair another)
            {
                if (intValue < another.intValue)
                    return -1;
                else if (intValue > another.intValue)
                    return 1;
                return 0;
            }
        }
        #endregion

        public int Draw(UsableTypes uTypes)
        {
            EditorGUILayout.LabelField("Class: ", GUILayout.Width(40));
            EditorGUI.BeginChangeCheck();
            currText = EditorGUILayout.TextField(GUIContent.none, currText, GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            if (EditorGUI.EndChangeCheck())
            {
                changed = true;
            }
            List<EnumStringValuePair> enumList = GetEnumList(uTypes);
            List<GUIContent> enumStrList = new List<GUIContent>(enumList.Count);
            for (int i = 0; i < enumList.Count; ++i)
            {
                enumStrList.Add(enumList[i].strValue.guiContent);
            }
            int selectedIndex = 0;
            for (int i = 0; i < enumList.Count; ++i)
            {
                if (enumList[i].intValue == enumValueIndex)
                {
                    selectedIndex = i;
                    break;
                }
            }
            selectedIndex = EditorGUILayout.Popup(GUIContent.none, selectedIndex, enumStrList.ToArray(), GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            if (enumList.Count > selectedIndex)
            {
                enumValueIndex = enumList[selectedIndex].intValue;
                value = enumList[selectedIndex].strValue;
            }

            return selectedIndex;
        }
    }
}