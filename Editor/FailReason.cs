namespace com.FDT.Common.Editor
{
    public enum FailReason
    {
        NONE = 0,
        UNKNOWN = 1,
        DOUBLE_DOT = 2,
        SPACE = 3,
        ENDSWITH_DOT = 4,
        STARTSWITH_DOT = 5,
        CONTAINS_RESERVED = 6
    }
}
