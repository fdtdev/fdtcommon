﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using com.FDT.Common.ReloadedScriptableObject;

namespace com.FDT.Common.ReloadedScriptableObject.Editor
{
    [CustomEditor(typeof(ReloadedScriptableObject), true)]
    public class ReloadedScriptableObjectEditor : UnityEditor.Editor
    {
        ReloadedScriptableObject cTarget = null;
        protected virtual void OnEnable()
        {
            cTarget = target as ReloadedScriptableObject;
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            if (cTarget.ManualSerializationControls)
            {
                EditorGUI.BeginDisabledGroup(!Application.isPlaying);
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("SAVE", EditorStyles.miniButton))
                {
                    cTarget.SaveData();
                }
               // GUILayout.FlexibleSpace();
                if (GUILayout.Button("RESTORE", EditorStyles.miniButton))
                {
                    cTarget.LoadData();
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                EditorGUI.EndDisabledGroup();
                GUILayout.Space(18);
            }
            DrawDefaultInspector();
            serializedObject.ApplyModifiedProperties();
        }
    }
}