﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace com.FDT.Common.ReloadedScriptableObject.Editor
{
    //[InitializeOnLoad]
    public class ScriptableObjectReset
    {
        #if UNITY_2019_2_OR_NEWER
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        static void Init()
        {
            ResetData();
            RestoreAllInterestingStates();
            Application.quitting += Quit;
        }

        static void Quit()
        {
            Application.quitting -= Quit;
            SaveAllInterestingStates();
        }
        #else
        static ScriptableObjectReset()
        {
            EditorApplication.playModeStateChanged -= OnPlayStateChanged;
            EditorApplication.playModeStateChanged += OnPlayStateChanged;
        }
        static void OnPlayStateChanged(PlayModeStateChange pmsc)
        {
            // If exiting playmode, collect the state of all interesting objects
            if (pmsc == PlayModeStateChange.ExitingEditMode)
            {
                SaveAllInterestingStates();
            }
            else if (pmsc == PlayModeStateChange.EnteredEditMode)
            {
                ResetData();
                RestoreAllInterestingStates();
            }
                
        }
        #endif
        static void SaveAllInterestingStates()
        {
            var a = AssetDatabase.FindAssets("t:ReloadedScriptableObject");
            for (int i = 0; i < a.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(a[i]);
                ReloadedScriptableObject so = AssetDatabase.LoadAssetAtPath<ReloadedScriptableObject>(path);
                if (so != null)
                {
                    so.SaveData();
                }
            }
        }

        static void ResetData()
        {
            var a = AssetDatabase.FindAssets("t:ResetScriptableObject");
            for (int i = 0; i < a.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(a[i]);
                ResetScriptableObject so = AssetDatabase.LoadAssetAtPath<ResetScriptableObject>(path);
                if (so != null)
                {
                    so.ResetData();
                }
            }
        }
        static void RestoreAllInterestingStates()
        {
            var a = AssetDatabase.FindAssets("t:ReloadedScriptableObject");
            for (int i = 0; i < a.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(a[i]);
                ReloadedScriptableObject so = AssetDatabase.LoadAssetAtPath<ReloadedScriptableObject>(path);
                if (so != null)
                {
                    so.LoadData();
                }
            }
        }
    }
}